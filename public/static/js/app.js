document.addEventListener("DOMContentLoaded", function() {
  try {
    let app = firebase.app();
    let features = ["auth", "database", "messaging", "storage"].filter(
      feature => typeof app[feature] === "function"
    );

    var data;
    var beacon = '<div class="circle charm" id=""></div>';
    var canvas = $("#canvas");
    var width = canvas.width();
    var height = canvas.height();

    var coordinates = function(element) {
      element = $(element);
      var top = element.position().top / canvas.outerHeight();
      var left = element.position().left / canvas.outerWidth();
      var data = { tag: $(element).attr("id"), x: left, y: top };
      this.getData = function() {
        return data;
      };
    };

    var addStyle = function(data) {
      var top = (parseFloat(data.y) * 100).toString();
      var left = (parseFloat(data.x) * 100).toString();
      $(document.getElementById(data.tag)).css({
        top: top + "%",
        left: left + "%"
      });
    };

    var deleteBeacon = function(id) {
      var req = {
        type: "DELETE",
        dataType: "json",
        url:
          "https://us-central1-stivers-scavenger-hunt.cloudfunctions.net/app/api/location/AIzaSyAmxdj4CuNl2GsrjA0nrsWcujB1mkX2iwM/" +
          id
      };

      $.ajax(req);
    };

    var putRequest = function(key) {};

    var updateBeacon = function(key, index) {
      var $beacon = $(
        "<div class='circle charm' id='" +
          key +
          "'><img src='../static/media/Icons/" +
          key +
          ".png'/></div>"
      );
      canvas.append($beacon);
      addStyle(index);
      var data = new coordinates($beacon).getData();
      var req = {
        type: "PUT",
        dataType: "json",
        url:
          "https://us-central1-stivers-scavenger-hunt.cloudfunctions.net/app/api/location/AIzaSyAmxdj4CuNl2GsrjA0nrsWcujB1mkX2iwM/" +
          key,
        data: data,
        success: function(response, status) {
          console.log(status);
        }
      };
      $.ajax(req);
      $beacon.draggable({
        start: function() {
          new coordinates($beacon);
        },
        stop: function() {
          var data = new coordinates($beacon).getData();
          var req = {
            type: "PUT",
            dataType: "json",
            url:
              "https://us-central1-stivers-scavenger-hunt.cloudfunctions.net/app/api/location/AIzaSyAmxdj4CuNl2GsrjA0nrsWcujB1mkX2iwM/" +
              key,
            data: data,
            success: function(response, status) {
              console.log(status);
            }
          };
          $.ajax(req);

          addStyle(data);
        }
      });
    };

    var req = {
      type: "GET",
      dataType: "json",
      url:
        "https://us-central1-stivers-scavenger-hunt.cloudfunctions.net/app/api/location/AIzaSyAmxdj4CuNl2GsrjA0nrsWcujB1mkX2iwM/admin/*",
      success: function(response, status) {
        var beacons = response.maplocations;
        Object.keys(beacons).forEach(function(key, index) {
          updateBeacon(key, beacons[key]);
        });
      }
    };

    $.ajax(req);
    $("#addButton")
      .unbind("click")
      .click(function() {
        var selectedBeacon = $(".beaconSelect option:selected").val();
        // Make sure a beacon is selected.
        if (selectedBeacon != "notChosen") {
          // Remove existing beacon of same type.
          if ($("#" + selectedBeacon).length != 0) {
            $("#" + selectedBeacon).remove();
          }
          var $beacon = $(
            "<div class='circle charm' id='" + selectedBeacon + "'></div>"
          );
          canvas = $("#canvas");
          // Initialize coordinates
          // Add beacon and coordinate display to DOM
          // Calculate current coordinates of draggable beacon
          var insert = {
            tag: selectedBeacon,
            x: "0",
            y: "0"
          };
          updateBeacon(selectedBeacon, insert);
        } else {
          alert("Please Select a Beacon.");
        }
      });

    // Removes beacon if selected type has been placed on map.
    $("#removeButton")
      .unbind("click")
      .click(function() {
        var selectedBeacon = $(".beaconSelect option:selected").val();
        // Make sure a beacon is selected.
        if (selectedBeacon != "notChosen") {
          // Remove existing beacon of same type.
          if ($("#" + selectedBeacon).length != 0) {
            deleteBeacon(selectedBeacon);
            $("#" + selectedBeacon).remove();
          } else {
            alert("Beacon is not placed.");
          }
        } else {
          alert("Please Select a Beacon.");
        }
      });
  } catch (e) {
    console.error(e);
    document.getElementById("load").innerHTML =
      "Error loading the Firebase SDK, check the console.";
  }
});
