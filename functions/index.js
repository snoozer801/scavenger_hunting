const functions = require('firebase-functions');
const express = require("express");
const app = express();
const routes = require("./routes");

const bodyParser = require("body-parser");
app.use(function(req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
   extended: true 
}));

app.use('/api', routes);

exports.app = functions.https.onRequest(app);
