const functions = require("firebase-functions");
const firebaseHelper = require("firebase-functions-helper");
const express = require("express");
const bodyParser = require("body-parser");
const admin = require("firebase-admin");
// admin.initializeApp(functions.config().firebase);

const db = admin.firestore();
const locationCollection = db.collection("maplocations");
const mapLocations = "maplocations";
var data = {};
//var setDoc = db.collection('locations').doc('init').set(data);
const config = require("../config/config.json");

var checkApiKey = key => {
  if (key !== config.apiKey) {
    res.status(403).send("Unauthenticated request");
    return false;
  }

  return true;
};

// Method: DELETE
// Delete Beacon
var deleteLocation = (req, res) => {
  if (!checkApiKey(req.params.apiKey)) {
    return;
  }

  firebaseHelper.firestore.deleteDocument(db, mapLocations, req.params.tag);
  res.status(200).send("Document deleted");
};

// Method: PUT
// Place Beacon
var setLocation = (req, res) => {
  if (!checkApiKey(req.params.apiKey)) {
    return;
  }

  const dbRef = db.collection(mapLocations).doc(req.params.tag);
  return dbRef
    .get()
    .then(doc => {
      if (!doc.exists) {
        firebaseHelper.firestore.createDocumentWithID(
          db,
          mapLocations,
          req.params.tag,
          req.body
        );
        res.status(200).send("New Beacon Created.");
      } else {
        firebaseHelper.firestore.updateDocument(
          db,
          mapLocations,
          req.params.tag,
          req.body
        );
        res.status(200).send("Existing Beacon Updated.");
      }
    })
    .catch(error => {
      console.log("Locations get error: " + error);
      res.status(500).send("Failed to set locations");
    });
};

// Method: GET
// /location/get/:apiKey/*
// Return Array of beacon locations
var getLocations = (req, res) => {
  if (!checkApiKey(req.params.apiKey)) {
    return;
  }

  locationCollection
    .get()
    .then(querySnapshot => {
      var locations = [];
      querySnapshot.forEach(docSnapshot => {
        var location = docSnapshot.data();
        locations.push({ tag: location.tag, x: location.x, y: location.y });
      });

      res.send({ locations: locations });
    })
    .catch(error => {
      console.log("Locations get error: " + error);
      res.status(500).send("Failed to retrieve locations");
    });
};

// Method: GET
// /location/get/:apiKey/admin/*
// Return JSON instead of Array
var getLocationsAdmin = (req, res) => {
  if (!checkApiKey(req.params.apiKey)) {
    return;
  }
  firebaseHelper.firestore
    .backup(db, mapLocations)
    .then(data => res.status(200).send(data))
    .catch(error => {
      console.log("Locations get error: " + error);
      res.status(500).send("Failed to retrieve locations");
    });
};

// Method: GET
// /location/get/:apiKey/:tag
// Return location of specific beacon
var getLocation = (req, res) => {
  if (!checkApiKey(req.params.apiKey)) {
    return;
  }

  firebaseHelper.firestore
    .getDocument(db, mapLocations, req.params.tag)
    .then(doc => res.status(200).send(doc))
    .catch(error => {
      console.log("Location get error: " + error);
      res.status(500).send("Failed to get location");
    });
};

module.exports = {
  locationSet: setLocation,
  locationGetAll: getLocations,
  locationGet: getLocation,
  locationDelete: deleteLocation,
  locationGetAllAdmin: getLocationsAdmin
};
