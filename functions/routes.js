const express = require("express");
const router = express.Router();

const functions = require("firebase-functions");
const admin = require("firebase-admin");

admin.initializeApp(functions.config().firebase);

const location = require("./Models/MapLocations");

router.put("/location/:apiKey/:tag", location.locationSet);
router.delete("/location/:apiKey/:tag", location.locationDelete);
router.get("/location/:apiKey/([*])", location.locationGetAll);
router.get("/location/:apiKey/admin/([*])", location.locationGetAllAdmin);
router.get("/location/:apiKey/:tag", location.locationGet);

module.exports = router;
